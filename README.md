Convert a Wikipedia article to plain text

## Usage
```console
$ python3 wk2plain.py <article>
```
