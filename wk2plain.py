#!/usr/bin/python3
import requests
import bs4
import sys

def send_request(url: str) -> bs4.BeautifulSoup:
    response = requests.get(url)
    html = bs4.BeautifulSoup(response.text, 'html.parser')
    return html

def get_title(html: bs4.BeautifulSoup) -> str:
    h1 = html.find('h1')
    title = h1.get_text()

    return title

def get_paras(html: bs4.BeautifulSoup) -> list[str]:
    paras = []
    p = html.find_all('p')
    for para in p:
        paras.append(para.get_text())

    return paras

def main():
    try:
        url = sys.argv[1]
        if 'wikipedia.org' in url:
            html = send_request(url)
            title = get_title(html)
            paras = get_paras(html)

            print(title)
            for p in paras:
                print(p)
        else:
            print('wk2plain: not a wikipedia article')
            return
    except IndexError:
        print('usage: wk2plain <article>')
        print('where article is any wikipedia article.')
        print('to send output to a file, redirect using \'> file\' on your shell')
        return

if __name__ == '__main__':
    main()
